window.onload = sum();

function sum() {

    // var fields = ["assinatura_nome", "assinatura_apelido", "assinatura_departamento", "assinatura_funcao", "assinatura_tel", "assinatura_ext", "assinatura_mobile", "assinatura_email"];
    // var form = document.assinatura_dados;

    var form = document.getElementById("form-assinatura"),
        inputs = form.querySelectorAll('input[data-target]'),
        companies = document.getElementsByName('company'),
        languages = document.getElementsByName('language'),
        signature = document.getElementById("pre-signature"),
        negocio = document.getElementById('assinatura-negocio'),
        negocioEn = document.getElementById('assinatura-negocio-en'),
        copyHTML = document.getElementById("select_signature_html"),
        copyTXT = document.getElementById("select_signature_txt"),
        generate = document.getElementById("button_submit"),
        companyImg = document.getElementById('company-logo'),
        companyUrl = document.getElementById('company-url'),
        companyUrl2 = document.getElementById('company-url2'),
        lblTel = document.querySelectorAll('.lbl-tel'),
        lblExt = document.querySelectorAll('.lbl-ext'),
        valExt = document.querySelectorAll('.val_ext'),
        lblMobile = document.querySelectorAll('.lbl-mobile'),
        //bannerImg = document.getElementById('banner_img');
        companyUrl3 = document.getElementById('company-url3');




    // Validate signature
    $('.js-choose-lang').on('change', function() {
        var el = $(this);
        if (el.val() == "PT") {
            $('.assinatura-negocio-en').removeClass("is-active");
            $('.assinatura-negocio-pt').addClass("is-active");

        } else {
            $('.assinatura-negocio-pt').removeClass("is-active");
            $('.assinatura-negocio-en').addClass("is-active");
        }
    });
    /*
        
        INPUTS

        <input data-target="name" data-type="text" ...>
        <input data-target="ext" data-type="tel" ...>

        TARGET

        <p data-target="name" ...>
        <p data-target="funcao" ...>
        <a data-target="ext" ...>

    */


    generate.addEventListener("click", generateSignature);
    copyHTML.addEventListener("click", copySignatureHTML);
    copyTXT.addEventListener("click", copySignatureTXT);

    function generateSignature() {

        var validForm = validateFields();


        // check if validate_form_fields return true to submit form
        if (validForm) {

            ////
            // Converter '00' em '+' no início do telefone e telemóvel
            var $phone = $('#assinatura_tel');
            $phone.val($phone.val().replace(/^00/, "+"));
            var $mobile = $('#assinatura_mobile');
            $mobile.val($mobile.val().replace(/^00/, "+"));
            ////

            var telefone = $phone.val(),
                telemovel = $mobile.val(),
                extensao = document.getElementById('assinatura_ext').value,
                negocioVal = negocio.options[negocio.selectedIndex].value,
                negocioValEn = negocioEn.options[negocioEn.selectedIndex].value,
                negocios = document.querySelectorAll('.un-negocio'),
                companyName;

            // Estrutura Organizacional
            for (i = 0, max = negocios.length; i < max; i++) {
                if ($('.assinatura-negocio-pt').hasClass('is-active')) {
                    negocios[i].innerText = negocioVal;
                } else {
                    negocios[i].innerText = negocioValEn;
                }

            }

            // if there are no values on mobile, phone or email, it hides the elements
            if (telefone == null || telefone == "") {
                for (var i = 0, max = lblTel.length; i < max; i++) {
                    lblTel[i].style.display = 'none';
                    lblExt[i].innerHTML = lblExt[i].innerHTML.replace(/&nbsp;/g, '');
                }
            } else {
                for (var i = 0, max = lblTel.length; i < max; i++) {
                    lblTel[i].style.display = 'inline-block';
                    lblExt[i].innerHTML = lblExt[i].innerHTML.replace('', '&nbsp;');

                }
            }
            if (extensao == null || extensao == "") {
                for (var i = 0, max = lblExt.length; i < max; i++) {
                    lblExt[i].style.display = 'none';
                }

            } else {
                for (var i = 0, max = lblExt.length; i < max; i++) {
                    lblExt[i].style.display = 'inline-block';
                }
            }
            if (telemovel == null || telemovel == "") {
                for (var i = 0, max = lblMobile.length; i < max; i++) {
                    lblMobile[i].style.display = 'none';
                }

            } else {
                for (var i = 0, max = lblMobile.length; i < max; i++) {
                    lblMobile[i].style.display = 'inline-block';
                }
            }

            // Change signature language on fields
            for (var i = 0, max = languages.length; i < max; i++) {
                if (languages[i].checked) {

                    languageSelected = languages[i].value;


                    switch (languageSelected) {

                        case "EN":
                            //bannerImg.setAttribute("src", "https://user.tap.pt/email/banner_en.png");
                            for (var i = 0, max = lblTel.length; i < max; i++) {
                                lblTel[i].innerHTML = lblTel[i].innerHTML.replace('Telefone', 'Telephone');
                            }
                            for (var i = 0, max = lblMobile.length; i < max; i++) {
                                lblMobile[i].innerHTML = lblMobile[i].innerHTML.replace('TelemÃ³vel', 'Mobile');
                            }
                            break;

                        case "PT":
                            //bannerImg.setAttribute("src", "https://user.tap.pt/email/banner_pt.png");
                            for (var i = 0, max = lblTel.length; i < max; i++) {
                                lblTel[i].innerHTML = lblTel[i].innerHTML.replace('Telephone', 'Telefone');
                            }
                            for (var i = 0, max = lblMobile.length; i < max; i++) {
                                lblMobile[i].innerHTML = lblMobile[i].innerHTML.replace('Mobile', 'TelemÃ³vel');
                            }
                            break;

                    }

                }
            }



            // Change company Image depending on the company you choose
            for (var i = 0, length = companies.length; i < length; i++) {
                if (companies[i].checked) {

                    companyName = companies[i].value;

                    switch (companyName) {
                        case "TAP":
                            companyImg.setAttribute("src", "../Content/Images/logo_tap.png");
                            companyImg.setAttribute('width', '164');
                            companyImg.setAttribute('height', '43');
                            companyUrl.setAttribute('href', 'http://flytap.com');
                            companyUrl2.setAttribute('href', 'http://flytap.com');
                            companyUrl2.innerText = "flytap.com";
                            companyUrl3.setAttribute('href', 'http://victoria.flytap.com');
                            companyUrl3.innerText = "victoria.flytap.com";
                            break;

                        case "UCS":
                            companyImg.setAttribute("src", "https://user.tap.pt/email/logo_ucs.png");
                            companyImg.setAttribute('width', '148');
                            companyImg.setAttribute('height', '35');
                            companyUrl.setAttribute('href', 'http://flytap.com');
                            companyUrl2.setAttribute('href', 'http://flytap.com');
                            companyUrl2.innerText = "flytap.com";
                            companyUrl3.setAttribute('href', 'http://victoria.flytap.com');
                            companyUrl3.innerText = "victoria.flytap.com";
                            break;

                        case "ME":
                            companyImg.setAttribute("src", "https://user.tap.pt/email/logo_tapme.png");
                            companyImg.setAttribute('width', '77');
                            companyImg.setAttribute('height', '16');
                            companyUrl.setAttribute('href', 'https://www.tap-mro.com');
                            companyUrl2.setAttribute('href', 'http://flytap.com');
                            companyUrl2.innerText = "flytap.com";
                            companyUrl3.setAttribute('href', 'https://www.tap-mro.com');
                            companyUrl3.innerText = "tap-mro.com";
                            break;

                        case "PGA":
                            companyImg.setAttribute("src", "https://user.tap.pt/email/logo_pga.png");
                            companyImg.setAttribute('width', '130');
                            companyImg.setAttribute('height', '55');
                            companyUrl.setAttribute('href', 'https://www.portugalia-airlines.pt');
                            companyUrl2.setAttribute('href', 'http://flytap.com');
                            companyUrl2.innerText = "flytap.com";
                            companyUrl3.setAttribute('href', 'https://www.portugalia-airlines.pt');
                            companyUrl3.innerText = "portugalia-airlines.pt";
                            break;


                    }


                    break;
                }
            }


            for (var i = 0, max = valExt.length; i < max; i++) {
                valExt[i].innerText = extensao;


            };

            for (var i = 0; i < inputs.length; i++) {

                var input = inputs[i],
                    type = input.getAttribute('data-type'),
                    targets = signature.querySelectorAll('[data-target="' + input.getAttribute('data-target') + '"]');


                switch (type) {
                    case "text":
                        for (var j = 0; j < targets.length; j++) {
                            targets[j].innerText = input.value;
                        }
                        break;

                    case "tel":
                        for (var g = 0; g < targets.length; g++) {
                            targets[g].innerText = input.value;
                            targets[g].href = 'tel:' + input.value;
                        }
                        break;

                    case "email":
                        for (var a = 0; a < targets.length; a++) {
                            var prefix = targets[a].getAttribute('data-prefix');
                            targets[a].innerText = input.value;
                            targets[a].href = prefix + input.value;
                        }
                        break;

                }

                generate.removeEventListener("click", generateSignature);

                form.style.display = 'none';
                signature.style.display = 'block';

            };

            // Colocar o focus no inÃƒÂ­cio do form
            $('html, body').animate({
                scrollTop: $('.content--assinatura').offset().top
            }, 300);

        } else {

            // Scroll to first error on submit fail
            $('html, body').animate({
                scrollTop: $('[data-validate].error').offset().top - 40
            }, 300);
            // Focus first error on submit fail
            if (!isMobileWidth) {
                $('[data-validate].error')[0].focus();
            }
        }




    }



    function copySignatureHTML() {

        var clipboardHTML = new Clipboard('#select_signature_html', {
            target: function() {
                return document.getElementById("a_html");
            }
        });


    }

    function copySignatureTXT() {

        var clipboardTXT = new Clipboard('#select_signature_txt', {
            target: function() {
                return document.getElementById("signature_txt");
            }
        });

    }



    document.getElementById('editar_assinatura').onclick = function() {
        form.style.display = 'block';
        signature.style.display = 'none';

        // Colocar o focus no inÃƒÂ­cio do form
        $('html, body').animate({
            scrollTop: $('.content--assinatura').offset().top
        }, 300);


        generate.addEventListener("click", generateSignature);
        return false;
    }

    document.getElementById('editar_assinatura_txt').onclick = function() {
        form.style.display = 'block';
        signature.style.display = 'none';

        // Colocar o focus no inÃ­Â­cio do form
        $('html, body').animate({
            scrollTop: $('.content--assinatura').offset().top
        }, 300);

        generate.addEventListener("click", generateSignature);
        return false;
    }

};


////
//
jQuery(document).ready(function($) {

    $('#tpVideoLinkOutlook').click(function(evt) {
        $('.tp-video-wrapper').show("slow");
        flowplayer().play("https://uptodate.tap.pt/SD/html/gerador_assinaturas/Content/Media/Assinatura Email TAP - Outlook.mp4");
    });
    $('#tpVideoLinkWebmail').click(function(evt) {
        $('.tp-video-wrapper').show("slow");
        flowplayer().play("https://uptodate.tap.pt/SD/html/gerador_assinaturas/Content/Media/Assinatura Email TAP - Webmail.mp4");
    });
    $('.tp-video-wrapper button.close').click(function(evt) {
        flowplayer().stop()
        $('.tp-video-wrapper').hide("slow");
    });
}); // Doc Ready