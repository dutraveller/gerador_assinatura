// Error messages
validateRequired = "Informação Obrigatória.";
validateCreditcardRequired = "Select your credit card.";
validateEmailMessage = "Verifique o seu email";
validateEmailConfirmationMessage = "Emails doesn't match.";
validatePhoneMessage = "Verifique o seu número de telefone";
validateTPNumberMessage = "The TP number you inserted is not valid.<br>Insert a 9 digits number.";
validatePINMessage = "The PIN you inserted is not valid.";
validatePasswordMessage = "The Password you inserted is not valid.";
validateCIPMessage = "The CIP number you inserted is not valid.";
validateDatepickerMessage1 = "The date you inserted is not valid.";
validateDatepickerMessage2 = "This date is not available.";
validateDatepickerMessage3 = "The date is not available.";
validatePromocodeMessage = "This promotion code can't be used for the selected route or dates."
validateHazmat = "Please accept the hazmat restrictions and baggage rules."


var validate_form_fields;
/******************** Form Validations *******************/

function createErrorLabel($this, message) {

    // get variables
    var id = $this.attr('id');
    var labelledby = id + '-tip';
    var field = $this.closest('.form-field');

    if($this.hasClass("validate-multiple-inputs")){
        $this = $this.find('input');
    }

    // set aria attributes for accessibility purposes
    $this.attr({
        'aria-labelledby': labelledby,
        'aria-invalid': 'true'
    }).addClass('error');

    // check if error doesn't exists yet
    if ( $('#' + labelledby).length < 1 ) {

        // create error markup
        field.append('<p class="label-error" id="'+ labelledby +'" aria-hidden="false">'+ message +'</p>');
    }
    setTimeout(function() {
        field.find('.label-error')
            // set error message
            .html(message)
            // show error
            .addClass('show')
            // set aria aria-hidden to false for accessibility purposes
            .attr('aria-hidden', 'false');
    }, 1);

    validate_form_fields = false;

}
function removeErrorLabel(el) {

    // get variables
    var $this = el;
    var id = $this.attr('id');
    var labelledby = id + '-tip';

    if($this.hasClass("validate-multiple-inputs")) {
        $this = $this.find('input');
    }

    // set aria attributes for accessibility purposes
    $this.attr({
        'aria-invalid': 'false'
    }).removeClass('error').addClass('valid');

    // remove error label (must be removed instead of hidded, for accessibility purposes)
    $this.closest('.form-field').find('.label-error').remove();
    /*if ( $('#' + labelledby).length > 0 ) {
        $this.closest('.form-field').find('.label-error')
            // hide error
            .removeClass('show')
            // set aria aria-hidden to true for accessibility purposes
            .attr('aria-hidden', 'true');
    };*/
}


// validate Email
function validateEmail(el) {

    // get variables
    var $this = el;
    var value = el.val();
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // email regex

    // validate email
    if ( regex.test(value) ) {

        removeErrorLabel($this);
    }
    else {

        // error message
        var message = validateEmailMessage;

        createErrorLabel($this, message);
    }
}
// validate Email Confirmation
function validateEmailConfirmation(el) {

    // get variables
    var $this = el;
    var value = el.val();
    var email = el.closest('fieldset').find('.js-email-match').val();

    // Validate phone number
    if ( value == email ) {

        removeErrorLabel($this);

        // add checked icon
        $this.addClass('valid');
    }
    else {

        // error message
        var message = validateEmailConfirmationMessage;

        createErrorLabel($this, message);

        // remove checked icon
        $this.removeClass('valid');
    }
}
// validate Phone (TODO: FIX REGEX)
function validatePhone(el) {

    // get variables
    var $this = el;
    var value = el.val();
    var regex = /[1-9][0-9]{8}/; // phone number regex (note: this regex is extremely incomplete)

    // Validate phone number
    if (regex.test(value) ) {

        removeErrorLabel($this);
    }
    else {

        // error message
        var message = validatePhoneMessage;

        createErrorLabel($this, message);
    }
}
// validate TP Number
function validateTPNumber(el) {

    // get variables
    var $this = el;
    var value = el.val().replace(/ /g,'');
    var regex = /^[0-9]*$/; // only number regex

    // Validate TP number
    if (regex.test(value) && value.length == 9) {
        removeErrorLabel($this);
    }
    else {

        // error message
        var message = validateTPNumberMessage;

        createErrorLabel($this, message);
    }
}
// validate PIN
function validatePIN(el) {

    // get variables
    var $this = el;
    var value = el.val();
    var regex = /^[0-9]*$/; // only number regex

    // Validate PIN
    if (regex.test(value) && value.length >= 4) {
        removeErrorLabel($this);
    }
    else {

        // error message
        var message = validatePINMessage;

        createErrorLabel($this, message);
    }
}
// validate Password
function validatePassword(el) {

    // get variables
    var $this = el;
    var value = el.val();

    // Validate PIN
    if (value.length >= 6 && value.length < 50) {
        removeErrorLabel($this);
    }
    else {

        // error message
        var message = validatePasswordMessage;

        createErrorLabel($this, message);
    }
}

// validate CIP
function validateCIP(el) {

    // get variables
    var $this = el;
    var value = el.val();

    // Validate CIP
    if (value.length === 6) {

        removeErrorLabel($this);
    }
    else {

        // error message
        var message = validateCIPMessage;

        createErrorLabel($this, message);
    }
}
// datepicker
function validateDatepicker(el) {

    // get variables
    var $this = el;
    var inputDate = $this.val();

    var todayDate = new Date();
    todayDate     = todayDate.setHours(0,0,0,0);

    var nextYearDate = new Date();
    nextYearDate     = nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);

    //Check if valid
    var inputDateValid = moment(inputDate, dateFormats).isValid(); // true

    //Check if valid from/to
    var inputFromDateValid = moment($('#datepicker-from').val(),  dateFormats).isValid(); // true
    var inputToDateValid = moment($('#datepicker-to').val(),  dateFormats).isValid(); // true

    var inputDateTimestamp = false;
    if(todayDate && inputDateValid){
        //Convert date to timestamp format for camparsion with today's date
        inputDateTimestamp = moment(inputDate,  dateFormats).format(dateFormatInputMomento);
        inputDateTimestamp = $.datepicker.parseDate(dateFormatInput, inputDateTimestamp).getTime();
    }
    //If valid
    //If Outbond date is today or after today
    //If Outbond date is previous then one year ahead
    //Then Handle the entered date value
    if(!inputDateValid){
        // error message
        var message = validateDatepickerMessage1;
        createErrorLabel($this, message);
    }
    else if(inputDateTimestamp < todayDate){
        // error message
        var message = validateDatepickerMessage2;
        createErrorLabel($this, message);
    }
    else if(inputDateTimestamp > nextYearDate){
        // error message
        var message = validateDatepickerMessage3;
        createErrorLabel($this, message);
    }
    else {
        removeErrorLabel($this);
    }
}

//TEMP
//Validate promo code
function validatePromocode(el) {

    // get variables
    var $this = el;
    var value = el.val();

    // Validate promo code
    if (value == "redesign") {
         removeErrorLabel($this);
       // console.log($(this).closest('.promotion-code').hasClass('secondary-container'));
         $this.closest('.promotion-code').addClass('promotion-code-activated-added');
    }
    else {

        // error message
        var message = validatePromocodeMessage;

        createErrorLabel($this, message);
    }
}


function formValidation(el) {
    // get variables
    var $this = el;
    var message;
    var validations = el.data('validate'); // get validate options (array)
//console.log(el)

    console.log(validations);

    if(el.attr("aria-hidden") == 'true'){
        return false;
    }

    // check if is datepicker input round
    if(el.hasClass("datepicker-input-round")){

        if ( el.val() === '' ) {
            var message = validateRequired;

            $(".date-input-mobile").addClass("error");
            $(".datepicker-mobile-error").text(message).addClass('show');
            createErrorLabel($this, message);
        }
        else if ( !el.closest('.input-data').hasClass('filled-valid') ) {
            var message = "Check your dates";

            $(".date-input-mobile").addClass("error");
            $(".datepicker-mobile-error").text(message).addClass('show');
            createErrorLabel($this, message);
        }

    }
    // check if is airport-autocomplete
    else if (el.hasClass("airport-autocomplete")){

        if(el.siblings(".iata-code").text() == ""){
            if(el.val() == ""){
                var message = validateRequired;
                createErrorLabel($this, message);
            }
            else {
                var message = "Airport not found";
                createErrorLabel($this, message);
            }
        }
    }
    // check if it's a group of inputs
    else if(el.hasClass("validate-multiple-inputs")){


        var validateMultipleError = false;
        // check if fields are empty
        el.find('input').each(function () {
            if ($(this).val() === '') {
                validateMultipleError = true;
            }
        });

        if (validateMultipleError) {

            // error message
            var message = validateRequired;

            createErrorLabel($this, message);
        }
        else{
            removeErrorLabel($this);
        }
    }
    // check if is a radio button
    else if (el.hasClass("radio-buttons-wrap")){

        var radios_checked = false;
        removeErrorLabel($this);

        el.find('.radio-input').each(function(){
            var $this = $(this);
            $this.removeClass('error');
            if ($this.is(':checked') ) {
                radios_checked = true;
            }
        });

        if(!radios_checked){

            el.find('.radio-input').addClass('error');
            var message = validateRequired;

            if (el.hasClass('creditcard')) {
                // error message for Credit Card type
                var message = validateCreditcardRequired;
            }
            createErrorLabel($this, message);
        }
    }
    // check if is a checkbox
    else if (el.hasClass("checkbox-wrapper")){

        removeErrorLabel($this);
        var $checkbox = el.find('.checkbox');
        if ($checkbox.is(":checked")) {
            $checkbox.removeClass('error');
        }
        else {
            $checkbox.addClass('error');
            var message = validateRequired;
            createErrorLabel($this, message);
        }
    }
    else if (el.hasClass("phone")) {
        console.log("Yupieeeee");
    }
    // check if data-validate has value "required"
    else if ( $.inArray('required', validations) != -1) {
        
        // check if field is empty
        if (el.val() === '') {

            // error message
            var message = validateRequired;

            createErrorLabel($this, message);
        }
        else {

            // check if data-validate has value "email"
            if ($.inArray('email', validations) != -1) {

                validateEmail(el);
            }
            else if ($.inArray('email-confirmation', validations) != -1) {

                validateEmailConfirmation(el);
            }
            else if ($.inArray('phone', validations) != -1) {

                validatePhone(el);
            }
            else if ($.inArray('tp-number', validations) != -1) {

                validateTPNumber(el);
            }
            else if ($.inArray('pin', validations) != -1) {

                validatePIN(el);
            }
            else if ($.inArray('password', validations) != -1) {

                validatePassword(el);
            }
            else if ($.inArray('cip', validations) != -1) {

                validateCIP(el);
            }
            else if ($.inArray('datepicker', validations) != -1) {

                validateDatepicker(el);
            }
            else {
                removeErrorLabel($this);
            }
        }
    }
    // check if data-validate has value "one-required"
    else if ( $.inArray('one-required', validations) != -1) {

        // check if is a checkbox, and at least one is checked
        if (el.hasClass("checkbox-group-wrapper")){
            var checkbox_checked = false;
            removeErrorLabel($this);

            el.find('.checkbox').each(function(){
                var $this = $(this);
                $this.removeClass('error');
                if ($this.is(':checked') ) {
                    checkbox_checked = true;
                }
            });

            if(!checkbox_checked){

                el.find('.checkbox').addClass('error');
                var message = validateRequired;

                createErrorLabel($this, message);
            }
        }
    }
}

// On click/submit, check if form is valid
$('.js-validate-promotion-code').on('click', function(e) {
    e.preventDefault();
    validatePromocode($('#promocode-number'));
});

// On click/submit, check if form is valid
$('.js-validate-form').on('click', function(e) {
    e.preventDefault();

    var validForm = validateFields();


    // check if validate_form_fields return true to submit form
    if (validForm) {
        $('.js-validate-form').submit();
    }
    else {
        // Scroll to first error on submit fail
        $('html, body').animate({
            scrollTop: $('[data-validate].error').offset().top - 40
        }, 300);
        // Focus first error on submit fail
        if(!isMobileWidth){
            $('[data-validate].error')[0].focus();
        }
    }
});
// Validate every fields on page
function validateFields() {
    validate_form_fields = true;

    $('.form-field').find('[data-validate]:visible').each(function() {
        
        var el = $(this);
        
        if(el.css('visibility') !== 'hidden'){
            // Validate each field
            formValidation(el);

        }
    });

    return validate_form_fields;
}

// Validate each field on blur
$('.form-field').on('blur change', '[data-validate]', function() {
    var el = $(this);
    if(el.hasClass('error') && (!el.hasClass('airport-autocomplete') || !el.hasClass('datepicker-input-single') || !el.hasClass('datepicker-input-round')) || $.inArray('onblur', el.data('validate')) != -1){
        var validations = el.data('validate'); // get validate options (array)
        formValidation(el);
    }
});

// Validate form field on keyboard up
$('.form-field').on('keyup', '[data-validate]', function() {
    var el = $(this);
    if(el.hasClass('error') && (!el.hasClass('airport-autocomplete') || !el.hasClass('datepicker-input-single') || !el.hasClass('datepicker-input-round'))){
        if ( el.hasClass('error') || el.hasClass('valid')) {
            var validations = el.data('validate'); // get validate options (array)
            formValidation(el);
        }
    }
});

// Validate airport field on blur
$('.flight-search-container').on("blur", ".airport-autocomplete", function(){
    var el = $(this);

    setTimeout(function(){
            if(el.siblings(".iata-code").text() == "" && el.val() != "" ){
                var message = "Airport not found";
                createErrorLabel(el, message);
            }
        //}
    }, 250);
});

// Validate airport field on change
function validateAirportError(el) {
    if(el.siblings(".iata-code").text() != ""){
        removeErrorLabel(el);
    }
}

// Validate datepicker field
function validateDatepickerError(el) {
    removeErrorLabel(el);
    $(".date-input-mobile").removeClass("error");
    $(".datepicker-mobile-error").removeClass('show');
}

