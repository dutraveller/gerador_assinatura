var WaitingPageAccessibilityMessage = "Please wait";
var WaitingPageRemovedAccessibilityMessage = "Page loaded";


// Get url Parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


// TEMP - Display cookie policy or browser update notifications
var url_notification_parameter = getParameterByName('notification');
if ( url_notification_parameter === 'cookiePolicy') {
    $('.notification-cookie-policy').removeClass('hidden');
}
/*else if (url_notification_parameter === 'updateBrowser') {
    $('.notification-update-browser').removeClass('hidden');
}*/


function ariaLiveReader(message) {
    $(".accessibility-alert-box").text(message);
}


// mobile-only
var isMobileWidth = false;
if (document.body.clientWidth < 781) {
    isMobileWidth = true;
}

// small-only
var isSmallWidth = false;
if (document.body.clientWidth < 641) {
    isSmallWidth = true;
}

/////// Browser notifications

// request permission on page load
/*
document.addEventListener('DOMContentLoaded', function () {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
});
*/

function browserNotification(message) {
    if (("Notification" in window)) {
        Notification.requestPermission(function(permission){
            var notification = new Notification("TAP Portugal", {
                body: message,
                icon: 'ico/apple-touch-icon-120x120.png'
            });
            setTimeout(function(){
                notification.close();
            }, 5000);
        });
    }

}


// Get url hashtag and open correspondent component
var url_hashtag = window.location.hash.substr(1);
if (url_hashtag != "") {
    $('[href="#'+ url_hashtag +'"]').click();
}



/////// Window resize event

$( window ).resize(function() {
    // mobile-only
    if (document.body.clientWidth < 781) {
        isMobileWidth = true;
    }
    else{
        isMobileWidth = false;
    }
    // small-only
    if (document.body.clientWidth < 641) {
        isSmallWidth = true;
    }
    else{
        isSmallWidth = false;
    }
    //Hide tooltips
    hideTooltip();

    //Hide dropdown boxes
    hideDropdowns();
});

// Disable Scroll
function disableScroll() {
    $('html').addClass('no-scroll');
}
// Enable Scroll
function enableScroll() {
    $('html').removeClass('no-scroll');
}


/////// Clear inputs
$(document).on( "keyup change paste input", ".input-clear input", function(){
    showHideClear($(this));
});

function showHideClear($this){
    if ($this.val() == "") {
        $this.siblings(".clear").hide();
        if($this.parent().hasClass('input-data-from') || $this.parent().hasClass('input-data-to')){
            if($('#datepicker-from').val() == "" && $('#datepicker-to').val() == ""){
                $this.parent().parent().siblings(".clear-datepicker-mobile").hide();
            }
        }
    }
    else {
        $this.siblings(".clear").show();
        if($this.parent().hasClass('input-data-from') || $this.parent().hasClass('input-data-to')){
            if($('#datepicker-from').val != "" || $('#datepicker-to').val != ""){
                $this.parent().parent().siblings(".clear-datepicker-mobile").show();
            }
        }
    }
}

$(document).on("click", ".input-clear .clear", function(e) {
    clearInput($(this));
});

function clearInput($this) {
    setTimeout(function(){
        $this.hide().siblings("input").val('').change().focus();
    }, 200);
}

// ===== Header ===================================================================

//Header (MOBILE) - Open/close Menu

    $("#menu-button").click( function(){
        if( $(".header-login-info").hasClass('open') ){
            $(".header-login-info").removeClass('open');
            $("#menu-overlay").fadeOut();
        }
        else{
            $(".header-login-info").addClass('open');
            $("#menu-overlay").fadeIn();
        }
    });
    $("#menu-overlay").click( function(){
        if( $(".header-login-info").hasClass('open') ){
            $(".header-login-info").removeClass('open');
            $("#menu-overlay").fadeOut();
        }
    });



//Header - Language
var $language_option = $('.header-language-option');
$language_option.on('click', function() {
    $language_option.removeClass('selected');
    $(this).addClass('selected');
});



// Trigger Header Options Mobile (Phase 1 - remove after)
/*
$('.js-header-options-trigger').on('click', function() {
    if ( $('.header-options-wrapper').hasClass('active') ) {
        $('.header-options-wrapper').removeClass('active');
    }
    else {
        $('.header-options-wrapper').addClass('active');
    }
});

// Trigger Header Options Mobile (Phase 1 - remove after)
$('#sl-country').on('change', function() {
    var $this = $(this);
    var countryCode = $this.val();
    $this.siblings('.selectbox-inner').find('.flag').attr('class', 'flag flag-'+countryCode);
    $('.js-header-options-trigger').find('.flag').attr('class', 'flag flag-'+countryCode).siblings('.country-code').text(countryCode);

});
*/


// Highlight current market
var url_market_parameter = getParameterByName('market');
var $countries_wrapper = $('.countries-wrapper');
if (url_market_parameter != '') {
    var market = url_market_parameter.toLowerCase();
    $countries_wrapper.find('.flag-'+ market).closest('.item').addClass('active');
}
$('.js-header-country-toggle').on('click', function() {
    var $this = $(this);

    // Toggle market menu
    var header_country = $('.header-country');
    if (header_country.hasClass('active')) {
        header_country.removeClass('active');
        $this.attr('aria-expanded', 'false');
    }
    else {
		if (!header_country.hasClass('disabled')){
			header_country.addClass('active');
            $this.attr('aria-expanded', 'true');
		}
        // scroll to current selection
        var current_selection_position = $countries_wrapper.find('.item.active').offset();
        var countries_wrapper_height = $countries_wrapper.innerHeight();
        $countries_wrapper.scrollTop(current_selection_position.top - (countries_wrapper_height/2));
        //console.log(current_selection_position.top)
        //console.log((countries_wrapper_height/2))
        //console.log(current_selection_position.top - (countries_wrapper_height/2))
    }

});

$('.js-header-login-toggle').on('click', function() {
    var $this = $(this);

    // Toggle market menu
    var header_login_info = $('.header-login-info');
    if (header_login_info.hasClass('active')) {
        header_login_info.removeClass('active');
        $this.attr('aria-expanded', 'false');
    }
    else {
        if (!header_login_info.hasClass('disabled')){
            header_login_info.addClass('active');
            $this.attr('aria-expanded', 'true');
        }
    }
});

$('.js-header-login-recover').on('click', function() {
    var modalId = $(this).attr('href');
    openModal(modalId, $(this));
});


// Continents Toggle
// Phase 2
$('.continent-toggle').on('click', function(e) {
    e.preventDefault();
    var $continent_item = $(this).closest('.continent-item');

    if ( $continent_item.hasClass('active') ) {

        if (document.body.clientWidth < 641) {
            $continent_item.removeClass('active').find('.countries-list').slideUp();
        }
    }
    else {
        if (document.body.clientWidth < 641) {
            $continent_item.addClass('active').find('.countries-list').slideDown();
        }
        else {
            $('.continent-item').removeClass('active');
            var target = $(this).attr('href');
            $(target).addClass('active');
        }
        $continent_item.addClass('active');
    }
});


// Trigger Header Options Mobile
$('.js-header-options-trigger').on('click', function() {
    if (document.body.clientWidth < 641) {
        $('.header-options').addClass('active');
    }
});

// Close header options on blur
$(document).on('click', function() {
    $('.header-country, .header-login-info').removeClass('active');
});
$('.countries-wrapper, .header-country, .header-login-info').on('click', function(e) {
    e.stopPropagation();
});


// Close Header Options Mobile
$('.countries-wrapper').on('click', '.modal-close', function() {
    $('.header-options, .header-login-info').removeClass('active');
});



// Countries Toggle
$('.country-item').on('click', function() {
    $('.country-item.active').removeClass('active');
    $(this).addClass('active');
});







// ===== 1 Search ===================================================================

//Multi-city Add Leg
var multicityCount = 2;

$('.add-flight').on('click', function(){
    //$('.multi-city-container').find('.search-leg:first-child').clone('true', 'true').find('.input').val('').end().appendTo('.new-leg');
    var $this = $(this);
    var $multicity = $('.multi-city-container');
    var length = $multicity.attr('data-length');

    if (length < 6) {

        // Add one leg
        $.ajax({
            type: 'GET',
            url: '../src/ajax/new-leg.html?v1',
            dataType: 'html',
            success: function(data){
                multicityCount = multicityCount+1;
                template = data.replace(/\{id\}/g, multicityCount);
                $('.new-leg').append(template);
                iniAutocomplete($('#select-partida-multicity-'+multicityCount));
                iniAutocomplete($('#select-chegada-multicity-'+multicityCount));
            }
        });

        // Add one to the count
        $multicity.attr('data-length', parseInt(length) + 1);
    }

    if (length == 5) {
        $this.addClass('fadeout');
    }

});

//Multi-city Delete Leg
$('.multi-city-container').on('click', '.js-multicity-leg-delete', function() {
    var $this = $(this);
    var $multicity = $('.multi-city-container');
    var length = $multicity.attr('data-length');

    // Remove one to the count
    $('.multi-city-container').attr('data-length', parseInt(length) - 1);

    // Remove one leg
    $(this).closest('.search-leg').remove();

    if (length == 6) {
        $('.multi-city-container').find('.add-flight').removeClass('fadeout');
    }
});

//Recent Searches
$('.recent-searches').on('click', '.delete-option', function() {
    $(this).closest('.itinerary-line').remove();
});
$('.more-recent-searches').on('click', function() {
    $(this).siblings('.itinerary-container').children('.itinerary-line').eq(0).clone(true, true).appendTo('.recent-searches > .itinerary-container');
});


// ===== Promocode ===========

// Enable/disable Promotion code
/*$('.promotion-code').on('change', '#check-promocode', function(){
    var value = $(this).attr('checked');
    var $input = $('#promocode-number');
    if (value=true) {
        $input.addClass('active').attr({
            'aria-disabled': true,
            'aria-hidden': true
        });
    }
    else {
        $input.removeClass('active').attr({
            'aria-disabled': false,
            'aria-hidden': false
        });
    }
});*/

$('#check-promocode').change(function() {

    var $this = $(this);
    //var $input = $(this, '.js-promocode-number');
    var $form = $('.promotion-code-form');
    if ($this.is(':checked')) {
        $form.addClass('active').attr('aria-hidden', 'false');
    }
    else {
        $form.removeClass('active').attr('aria-hidden', 'true');
    }
});

/*
$('#check-promocode').change(function() {

    var $this = $(this);
    var $input = $('#promocode-number').closest('.form-field');
    if ($this.is(':checked')) {
        $input.addClass('fadein').attr({
            'aria-disabled': true,
            'aria-hidden': true
        });
    }
    else {
        $input.removeClass('fadein').attr({
            'aria-disabled': false,
            'aria-hidden': false
        });
}
});
*/

//Add promotion code
/*$('.js-activate-promotion-code').on('click', function(){

    $(this).closest('.promotion-code').addClass('promotion-code-activated-added');
});*/

$('.js-remove-promotion-code').on('click', function(){
    $(this).closest('.promotion-code').removeClass('promotion-code-activated-added');
});


// ===== 2 Select + Multicity Resumes ===================================================================

// Toggle Search Resume dropdown
$('.btn-search-resume-dowpdown').on('click', function() {
    var $this = $(this);
    var $dropdown = $this.closest('.search-resume-inner').siblings('.search-resume-dropdown');
    if ($this.hasClass('active')) {
        $this.removeClass('active');
        $dropdown.removeClass('active');
    }
    else {
        $this.addClass('active');
        $dropdown.addClass('active');
    }

    // Close Modify Search, if open
    if ($('.modify-search').hasClass('active')) {
        $('.js-modify-search-btn').click();
    }
});

// Toggle Modify Search Dropdown
$('.js-modify-search-btn').on('click', function(e) {
    e.preventDefault();
    var $flight_options = $(this).closest('.search-resume');
    var $modify_search = $flight_options.find('.modify-search');
    if ( $flight_options.hasClass('modify-search-active') ) {
        $flight_options.removeClass('modify-search-active');
        $modify_search.removeClass('active').attr('aria-hidden', 'true');
    }
    else {
        $flight_options.addClass('modify-search-active');
        $modify_search.addClass('active').attr('aria-hidden', 'false');
    }
    if (isSmallWidth) {
        disableScroll();
    }

});



// ==== Old modify search (to be erased after Anixe change the code) =================================================
$('.js-modify-passengers-btn').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.modify-search').find('.js-modify-passengers').slideToggle();
});
$('.modify-search').on('click', function(e) {
    $('.js-modify-search').removeClass('active');
});

$('.modify-search').on('click', '.modal-close', function() {
    $(this).closest('.mobile-modal').removeClass('active').closest('.search-resume').removeClass('modify-search-active');
    enableScroll();
});

// ==== EOF Old modify search (to be erased after Anixe change the code) ===========================================


// Toggle filter Search Dropdown
$('.js-filter-search-btn').on('click', function(e) {
    e.preventDefault();
    var $filter_search_wrapper = $(this).closest(".filter-search-wrapper");
    var $filter_search = $('#filter-search');

    if (document.body.clientWidth > 640 && document.body.clientWidth < 1025) {
        $right_offset = ( $filter_search_wrapper.offset().left + $filter_search.innerWidth() ) - $(window).width() + 10;

        if($right_offset < 0) {
            $right_offset = 0;
        }

        $filter_search.css("margin-left", ($filter_search_wrapper.offset().left - $right_offset) + "px");
    }


    if ( $filter_search_wrapper.hasClass('filter-search-active') ) {
        $(this).removeClass('filter-search-active');
        $filter_search_wrapper.removeClass('filter-search-active');
        $filter_search.removeClass('active').attr('aria-hidden', 'true');
    }
    else {
        $(this).addClass('filter-search-active');
        $filter_search_wrapper.addClass('filter-search-active');
        $filter_search.addClass('active').attr('aria-hidden', 'false');
    }
    if (isSmallWidth) {
        disableScroll();
    }
});
$('.filter-search').on('click', function(e) {
    $('.js-filter-search').removeClass('active');
});
$("#filter-search").on("change", "input, select", function(){
    $filled = false;

    $("#filter-search").find("input").each(function() {
        if($(this).is(':checked')){
            $filled = true;
        }
    });

    $("#filter-search").find("select").each(function() {
        if($(this).val() != "0"){
            $filled = true;
        }
    });

    if($filled && !$("#filter-search").parent(".filter").hasClass("filled")){
        $("#filter-search").parent(".filter").addClass("filled");
    }
    else if(!$filled && $("#filter-search").parent(".filter").hasClass("filled")){
        $("#filter-search").parent(".filter").removeClass("filled");
    }
});

$("#sort-selectbox").on("change", function(){
    var $sort = $(this).val().split('_');
    $('.sort-item.active').removeClass('active asc desc');

    $('.sort-item[data-sort="' + $sort[0] + '"]').addClass('active ' + $sort[1]);

});

$('.filter-search-wrapper .btn-primary, .filter-search-wrapper .modal-close').click(function(e) {
    $('.js-filter-search-btn').click();
});


// Close Modify Search and Search Resume Dropdown on blur
$('body').on('click', function() {
    if ($('.modify-search').hasClass('active')) {
        $('.js-modify-search-btn').click();
    }
    else if ($('.btn-search-resume-dowpdown').hasClass('active')) {
        $('.btn-search-resume-dowpdown').click();
    }

    hideDropdowns();
});
$('.js-modify-search-btn, .modify-search, .btn-search-resume-dowpdown, .search-resume-dropdown, .js-filter-search-btn, .filter-search').on('click', function (e) {
     e.stopPropagation();
});


function hideDropdowns() {
    if ($('.filter-search').hasClass('active')) {
        $('.js-filter-search-btn').click();
    }
}






// ===== General  ==============================================

// Price Breakdown mobile
$('.js-open-price-breakdown').on('click', function() {
    $(this).closest('.price-breakdown-container').find('.mobile-modal').addClass('active');
});
$('.price-breakdown-mobile').on('click', function(){
    $('.js-open-price-breakdown').click();
});


// Prevent Shopping Cart toggle on desktop
$(".js-mybooking-toggle").on('click', function() {
    if(isMobileWidth) {
        $(".booking-block:not(.removed)").slideToggle();
        $(this).toggleClass("active");
    }
});


//Timer bar - Session Expiring
var sessionMinutes = 10;
var warningMinutes = 2; //ending soon warning

timer(sessionMinutes);

$('.timer-bar-container .bar').animate({
      width: 0
    }, parseInt(sessionMinutes*60000), "linear", function() {
        $('#modal-session').addClass('active');
        $( ".container-topright" ).remove();
        browserNotification('Your session has expired.');
});


//Timer bar - Display minutes left
function timer(minutes) {
    var seconds = 60;
    var mins = minutes

    function tick() {
        if ($('#counter').length > 0) {
            var counter = document.getElementById("counter");
            var current_minutes = mins-1
            seconds--;
            counter.innerHTML = current_minutes.toString() /* + ":" + (seconds < 10 ? "0" : "") + String(seconds)*/;
        }

        if( seconds > 0 ) {
            setTimeout(tick, 1000);
        }

        else {
            //console.log(warningMinutes+" == "+current_minutes);
            if(warningMinutes == current_minutes){
                browserNotification('Your session will expire soon.');
            }
            if (mins > 1){
               setTimeout(function () { timer(mins - 1); }, 1000);
            }
        }
    }
    tick();
}


$(".delete-option").click(function(e) {
    e.preventDefault();
    $(this).closest(".container-topright").slideUp();
});





// SPLASH PAGE ================================================================

$('#splashpage-market').on('change', function() {
    var $language_select = $('.splashpage-language-wrapper');

    if ( $(this).val() == '') {
        $language_select.addClass('input-disabled');
    }
    else {
        $language_select.removeClass('input-disabled');
    }
});



//TEMP - Waiting pages random
function showWaitingPage() {
     var waitingContent = [{
        "imgsrc" : "Content/Images/waiting-pages/waiting-page-1.jpg",
        "title" : "Check-in",
        "text" : "Do it online or through mobile devices — choose what suits you best."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-2.jpg",
            "title" : "Digital Kiosk",
            "text" : "Choose your favourite periodicals and enjoy your in-flight reading in a digital format (the number of titles available depends on your chosen fare)"
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-3.jpg",
            "title" : "Included Meals",
            "text" : "Enjoy your flight, the meal is on us (except in tap|discount fares)."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-4.jpg",
            "title" : "Checked Luggage Included",
            "text" : "Leave nothing behind. Checked luggage is free (except in tap|discount fares)."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-5.jpg",
            "title" : "Customised Loyalty Programme",
            "text" : "Whether you are a Gold, Silver or Miles Client, you can win miles and trade them for flights, products or services."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-6.jpg",
            "title" : "Customised Loyalty Programme",
            "text" : "Start earning miles while you’re young!"
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-7.jpg",
            "title" : "Customised Loyalty Programme",
            "text" : "Mile after mile, you’ll be flying high with STEP. Let’s do it!"
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-8.jpg",
            "title" : "Business Class",
            "text" : "Exclusive and personalised service with maximum comfort so that you can work and rest in the clouds."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-9.jpg",
            "title" : "Protecting the environment",
            "text" : "When travelling with TAP you can compensate for your carbon dioxide emissions."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-10.jpg",
            "title" : "Tours and Activities",
            "text" : "We take you to the best destinations, with a vast selection of tourism activities."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-11.jpg",
            "title" : "Hotels",
            "text" : "Choose your home away from home for the best prices."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-12.jpg",
            "title" : "Rent-a-car",
            "text" : "We have offers for the full enjoyment of your destination."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-13.jpg",
            "title" : "Extra Luggage",
            "text" : "Buy your extra luggage online at a reduced price."
        },/*
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-14.jpg",
            "title" : "Time to Think",
            "text" : "We give you time to think — leave your reservation on hold with the same guaranteed price for the next 48 hours."
        },*/
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-15.jpg",
            "title" : "Destinations ",
            "text" : "We always have the best routes for business or pleasure."
        },
        {
            "imgsrc" : "Content/Images/waiting-pages/waiting-page-16.jpg",
            "title" : "Destinations ",
            "text" : "We always have the best routes for business or pleasure."
        }

    ];

    var myRandom = Math.floor(Math.random()*waitingContent.length);
    $('.waiting-img').attr('src', waitingContent[myRandom].imgsrc);
    $('.waiting-title').text(waitingContent[myRandom].title);
    $('.waiting-text').text(waitingContent[myRandom].text);

    $("#modal-waiting").addClass("active");
    $("html").addClass("no-scroll");

    // Accessibility purposes
    $(".header-container, .main-container").attr('aria-hidden', 'true'); // remove keyboad access to the page
    $('.accessibility-alert-box').append('<span>'+ WaitingPageAccessibilityMessage +'</span>');
    sayWaitingPageAccessibilityMessage();
}
function hideWaitingPage() {
	$("#modal-waiting").removeClass("active");
	$("html").removeClass("no-scroll");

    $(".header-container, .main-container").attr('aria-hidden', 'false'); // Add keyboad access to the page
}
function sayWaitingPageAccessibilityMessage() {
    setTimeout(function() {
        $('.accessibility-alert-box').empty();
        setTimeout(function() {
            $('.accessibility-alert-box').append('<span>'+ WaitingPageAccessibilityMessage +'</span>');
        }, 100);
        if ($("#modal-waiting").hasClass("active")) {
            sayWaitingPageAccessibilityMessage();
        }
        else {
            $('.accessibility-alert-box').empty();
            setTimeout(function() {
                $('.accessibility-alert-box').append('<span>'+ WaitingPageRemovedAccessibilityMessage +'</span>');
            }, 100);
        }
    }, 6000);
}



//TEMP - Dummy pages random

function showRandomDestination() {
    var dummyContent = [{
        "background" : "Content/Images/dummy-pages-content/amsterdam.jpg",
        "lettering" : "Content/Images/dummy-pages-content/amsterdam_lettering.png",
        "destination": "destinos.flytap.com/amsterdam",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Amsterdam"
    },
    {
        "background" : "Content/Images/dummy-pages-content/barcelona.jpg",
        "lettering" : "Content/Images/dummy-pages-content/barcelona_lettering.png",
        "destination": "destinos.flytap.com/barcelona",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Barcelona"
    },
    {
        "background" : "Content/Images/dummy-pages-content/casablanca.jpg",
        "lettering" : "Content/Images/dummy-pages-content/casablanca_lettering.png",
        "destination": "destinos.flytap.com/casablanca",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Casablanca"
    },
    {
        "background" : "Content/Images/dummy-pages-content/funchal.jpg",
        "lettering" : "Content/Images/dummy-pages-content/funchal_lettering.png",
        "destination": "destinos.flytap.com/funchal",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Funchal"
    },
    {
        "background" : "Content/Images/dummy-pages-content/london.jpg",
        "lettering" : "Content/Images/dummy-pages-content/london_lettering.png",
        "destination": "destinos.flytap.com/london",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/London"
    },
    {
        "background" : "Content/Images/dummy-pages-content/new-york.jpg",
        "lettering" : "Content/Images/dummy-pages-content/new-york_lettering.png",
        "destination": "destinos.flytap.com/new-york",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/New-York"
    },
    {
    "background" : "Content/Images/dummy-pages-content/paris.jpg",
    "lettering" : "Content/Images/dummy-pages-content/paris_lettering.png",
        "destination": "destinos.flytap.com/paris",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Paris"
    },
    {
        "background" : "Content/Images/dummy-pages-content/rio-de-janeiro.jpg",
        "lettering" : "Content/Images/dummy-pages-content/rio-de-janeiro_lettering.png",
        "destination": "destinos.flytap.com/rio-de-janeiro",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Rio-de-Janeiro"
    },
    {
        "background" : "Content/Images/dummy-pages-content/sal.jpg",
        "lettering" : "Content/Images/dummy-pages-content/sal_lettering.png",
        "destination": "destinos.flytap.com/sal",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Sal"
    },
    {
        "background" : "Content/Images/dummy-pages-content/sao-tome.jpg",
        "lettering" : "Content/Images/dummy-pages-content/sao-tome_lettering.png",
        "destination": "destinos.flytap.com/sao-tome",
        "destinationurl":"http://destinos.flytap.com/en/All-destinations/Destinations/Sao-Tome"
    }
    ];

    var myRandom = Math.floor(Math.random()*dummyContent.length);
    // var imageurl = "../../"+dummyContent[myRandom].background;
    // $('.dummy-img').style.background = "url('"+imageurl+"') no-repeat left top";
    $('.dummypage-content').css('background-image', 'url(' + dummyContent[myRandom].background + ')' );
    $('.dummypage-lettering').attr('src',dummyContent[myRandom].lettering);
    $('.dummypage-link').text(dummyContent[myRandom].destination).attr('href',dummyContent[myRandom].destinationurl);

}

//showWaitingPage();
//showRandomDestination();




// SVG fallback for IE
if (!Modernizr.svg) {
    $('svg').each(function() {
        var $this = $(this);
        var img_name = $this.data('img');
        if (typeof img_name !== typeof undefined && img_name !== false) {
            var img_classes = $this.attr('class');
            $this.before('<img class="'+ img_classes +'" src="Content/Images/ie/'+ img_name +'.png" alt="">');
        }
    });
}



// Alerts - for Accessibility
$('#alert-trigger').on('click', function(){
    showWaitingPage();
});


//Login
$('.js-user-login').on('click', function(){
    $('.header-login-info').removeClass('active');
    $('.user-options-login').addClass('hidden');
    $('.user-options-list').removeClass('hidden');
    $('.user-login-ttl').text("Mr. Francisco Cabral | 11.500 miles");
    $('.user-options__mobile--login').addClass('active');
});

//Logout
$('.js-user-logout').on('click', function(){
    $('.header-login-info').removeClass('active');
    $('.user-options-login').removeClass('hidden');
    $('.user-options-list').addClass('hidden');
    $('.user-login-ttl').text("Sign in");
    $('.user-options__mobile--login').removeClass('active');
});
