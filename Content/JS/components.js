/*
    Scripts of components used across the website
*/


var messageUsePointsExceeded = "You cannot use more than ";
var messageUsePointsLessThan10 = "You cannot use less than 10 ";



// BUTTONS ============================================================================================
// Trigger buttons using the Space key
$('body').on('keypress', 'a[role="button"]', function(e){

    var $this = $(this);
    if (e.keyCode === 32) { // check for Space key
        $this.trigger('click');
        return false;
    }
});
// Print button
$('.js-print').on('click', function() {
    window.print();
});



// == TABS ===========================

// version with links
$('.tab-label').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var tab_id = $this.attr('href');
    var modal = $this.closest('.modal-wrapper');

    if(typeof modal[0] != "undefined"){
        $('#' + modal[0].id + ' .tab-list > li').removeClass('current');
        $this.parent().addClass('current');

        $('#' + modal[0].id + ' .tab-content').removeClass('current');
        $(tab_id).addClass('current').focus();
    }else{
        $(' .tab-list > li').removeClass('current');
        $this.parent().addClass('current');

        $(' .tab-content').removeClass('current');
        $(tab_id).addClass('current').focus();
    }

    $('.products-line').addClass('is-active');

});
// version with radio btns
/*$('.tab-input').change(function() {
    var $this = $(this);
    var target_id = $this.attr('aria-controls');

    $('.tab-input').attr('aria-selected', 'false');
    $this.attr('aria-selected', 'true');

    $('.tab-content').removeClass('current');
    $('#'+ target_id).addClass('current').parent();
});*/



// TOGGLES ============================================================================================

// Toggle active class on Show More buttons
$('.show-more').on('click', function(){
    $(this).toggleClass('active');
});


// Table slide toggle
$('.tr-toggle .col-label').click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $dropdown = $this.closest('.tr-toggle').next('tr').find('.container-table-inner');

    if ( $this.hasClass('active') ) {
        $this.removeClass('active');
        $dropdown.removeClass('active').slideUp();
    }
    else {
        $this.addClass('active');
        $dropdown.addClass('active').siblings('.calculating-inner').slideDown();


        setTimeout(function() {
            $dropdown.slideDown().siblings('.calculating-inner').slideUp();
        }, 2000);
    }
});

// Full fare conditions toggle
$('.js-expand-panel').click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var href = $this.attr("href");
    $(href).slideToggle();
    if ( $this.hasClass("active") ) {
        $this.removeClass("active");
    } else {
        $this.addClass("active");
    }
});

// MODALS ============================================================================================

var current_modal_trigger;
var current_modal_top_offset;

function initModal() {
    $(document).off().on('click', '.open-modal', function() {

        //$('.modal-wrapper').removeClass('active'); // Prevent multiple modals at the same time
        var modalId = $(this).attr('href');
        openModal(modalId, $(this));
    });
}
function openModal(modalId, modalTrigger) {

    current_modal_top_offset = $(document).scrollTop(); // Global variable. So we know the current scoll possition, to set it again after the modal is closed

    if($(".modal.active").length) {
        closeModal();
    }

    $(modalId).addClass('active');
    $('.header-container, .main-container').attr('aria-hidden','true'); // mark the main page as hidden

    current_modal_trigger = modalTrigger; // Global variable. So we know what was the trigger to this modal and return the focus to it on modal close
    $('html').addClass('no-scroll');
}
function closeModal() {
    $('.modal-close, .js-modal-close').on('click', function(e) {
        e.preventDefault();

        $(this).closest('mobile-modal, .modal-wrapper').removeClass('active');

        // enable main content if no other modal is opened
        if (!$('.modal-wrapper').hasClass('active')) {
            $('html').removeClass('no-scroll'); // enable scroll
            $('.header-container, .main-container').attr('aria-hidden','false'); // mark the main page as visible
        }

        // remove modal hashtag from url
        window.location.hash="";

        // focus on modal trigger, for accessibility purposes
        setTimeout(function() {
            if(current_modal_trigger){
                current_modal_trigger.focus();
                $(document).scrollTop( current_modal_top_offset );
            }
        }, 40);
    });
}
initModal();
closeModal();

function closeActiveModal() {
    var $activeModal = $('.modal-wrapper.active');
    $activeModal.find('.js-modal-close').trigger('click');
}

// Close Modal on escape key press
$(document).on('keyup', function(e) {
    if (e.keyCode === 27) { // escape
        $('.modal-wrapper.active').find('.modal-close, .js-modal-close').click();
    }
});

$('.js-modal-back').on('click', function() {
    var $this = $(this);
    $this.closest('.mobile-modal').removeClass('active');
    $this.closest('.modal-wrapper').removeClass('active');
});

// Open mobile modal
function openMobileModal($this) {
    $this.siblings('.mobile-modal').addClass('active');
    $('html').addClass('no-scroll');
}
function closeMobileModal($this) {
    $this.closest('.mobile-modal').removeClass('active');
    $('html').removeClass('no-scroll');
}
$('.modal-toggle-mobile').on('click', function() {
    var $this = $(this);
    openMobileModal($this);
});
// Close mobile modal
$('.mobile-modal').on('click', '.modal-close, .js-modal-close', function() {
    var $this = $(this);
    closeMobileModal($this);
});



// BANNERS ============================================================================================

$('.js-open-banner-options-ccontainer').on('click', function() {
    $("#"+$(this).data("banner")).slideDown();
    $(this).fadeOut();
});


// DROPDOWNS ============================================================================================

// Accessibility dropdowns
$('.dropdown-toggle').off().on('keydown', function(e) {
    var $this = $(this);
    if (e.keyCode === 13) { // enter
        dropdownToggle($this);
    }
});
$('.dropdown-toggle').off().on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    dropdownToggle($this);
});

// Dropdown keyboard navigation
$('.dropdown li:last-child > .dropdown-item').on('keydown', function(e) {
    var $dropdown = $(this).closest('.dropdown');
    e.preventDefault();
    if (e.keyCode === 9) { // tab
        $dropdown.find('.dropdown-toggle').focus();
    }
});

$('.dropdown-item').on('keydown', function(e) {
    var $this = $(this);
    if (e.keyCode === 13) { // enter
        dropdownSelectItem($this);
    }
});
$('.dropdown-item').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    dropdownSelectItem($this, e);
});
function dropdownToggle($this) {
    var $dropdown = $this.closest('.dropdown');
    // if dropdown is open
    if ( $dropdown.hasClass('active') ) {
        // dropdown closes
        $dropdown.removeClass('active');
    }
    else {
        // dropdown open
        $dropdown.addClass('active');
    }
}

// When I choose an option from the dropdown
function dropdownSelectItem($this) {
    var txt = $this.text();
    var $dropdown = $this.closest('.dropdown');
    // Dropdown closes, change dropdown label and focus on it
    $dropdown.removeClass('active').find('.dropdown-toggle').text(txt).focus();
}



// TOOLTIPS ============================================================================================
/*var hideTooltipTimeout;
var autoHideTooltipTimeout;

$('body').append('<div role="tooltip" aria-hidden="true" class="tooltip"></div>');
var $tooltip = $('.tooltip');

function showTooltip(el, message, autohide) {

    // hide previous tooltip
    clearTimeout(hideTooltipTimeout);

    // get valiables
    var $this = el;
    var position = $this.offset();
    var data_type = $this.data('tooltip-type');
    var data_position = $this.data('tooltip-position');
    var data_align = $this.data('tooltip-align');
    var describedby = $this.attr('data-describedby');

    //set message
    if(typeof message === 'undefined'){
        message = $this.data('tooltip');
    }

    // set aria-describedby for accessibility purposes
    $this.attr('aria-describedby', describedby);

    $tooltip
        // create tooltip
        .html('<div class="tooltip-inner '+ ((data_type == undefined ) ? '' : data_type) +' tooltip-'+ ((data_position == undefined ) ? 'top' : data_position) +' tooltip-align-'+ ((data_align == undefined || data_position == 'left' || data_position == 'right') ? 'left' : data_align) + '">'+ message +'</div>')
        // show tooltip
        .addClass('fadein', function(){
            tooltipPosition(position, data_position, data_align, $tooltip, $this.width());
        })
        // aria attribute for accessibility purposes
        .attr('aria-hidden', 'false')
        // make tooltip readable when focus on trigger
        .attr('id', describedby);

    //auto hide tooltip
    if($this.attr('data-autohide')) {
        autohide = $this.data('autohide');
    }
    else if(typeof autohide === 'undefined') {
        autohide = 0;
    }
    if(autohide > 0){
        // hide previous tooltip
        clearTimeout(autoHideTooltipTimeout);
        autoHideTooltipTimeout = setTimeout(function() { hideTooltip(); }, autohide);
    }

    return false;
}
function hideTooltip() {
    $tooltip.removeClass('fadein').attr('aria-hidden', 'true');
    hideTooltipTimeout = setTimeout(function() {
        $tooltip.css({
            'top': '-9999px',
            'left': '-9999px'
        });
    }, 200);
}

// To remove ======
$('.tooltip-close').on('click', function(){
    $(this).closest('.fixed-tooltip').removeClass('fadein').attr('aria-hidden', 'true');
});
//    ======

function tooltipPosition(position, data_position, data_align, $tooltip, el_width) {
    if ( data_position == undefined || data_position == 'top' ) {
        if(el_width > 23){
           positionLeft = position.left - 6;
        }
        else{
           positionLeft = position.left - (23 - el_width/2);
        }

        $tooltip.css({
            'top': position.top - $tooltip.height(),
            'left': positionLeft
        });
    }
    else if (data_position == 'bottom') {
        $tooltip.css({
            'top': position.top + $this.height() + 48,
            'left': position.left - 6
        });
    }
    else if (data_position == 'left') {
        $tooltip.css({
            'top': position.top - $tooltip.height() + $this.height() + 30,
            'left': position.left - $tooltip.width() - 18
        });
    }
    else if (data_position == 'right') {
        $tooltip.css({
            'top': position.top - $tooltip.height() + $this.height() + 30,
            'left': position.left + $this.width() + 65
        });
    }

    if ( data_align == 'center') {
        $tooltip.css({
            'left': position.left - ($tooltip.width() / 2) + ($this.outerWidth() / 2)
        });
    }
}

// show tooltips on hover
$('.tooltip-hover').hover(function() {
    var el = $(this);
    showTooltip(el);
}).mouseout(function() {
    hideTooltip();
});
// show tooltips on click
$('.tooltip-toggle').on('click', function(e) {
    e.preventDefault();
    var el = $(this);
    showTooltip(el);
});
// show tooltips for screen readers
$('.tooltip-hover, .tooltip-toggle').focus(function() {
    var el = $(this);
    showTooltip(el);
});
*/


$('body').append('<div role="tooltip" class="tooltip"></div>');
var $tooltip = $('.tooltip');
var hideTooltipTimeout;

function showTooltip(el, message) {

    // hide previous tooltip
    clearTimeout(hideTooltipTimeout);

    // get valiables
    var $this = el;
    var position = $this.offset();
    var width = $this.width();
    var content_message = $this.find('.tooltip-content').html();
    var describedby = $this.attr('data-describedby');

    // set aria-describedby for accessibility purposes
    $this.attr('aria-describedby', describedby);

    $tooltip
        // create tooltip
        .html('<p class="tooltip-inner">'+ ((message === undefined) ? content_message : message) +'</p>')
        // show tooltip
        .addClass('active')
        .css({
            'top': position.top - $tooltip.height(),
            'left': position.left - (23 - width/2)
        })
        // make tooltip readable when focus on trigger
        .attr('id', describedby);
}
function hideTooltip() {
    hideTooltipTimeout = setTimeout(function() {
        $tooltip.removeClass('active');
    }, 2800);
}


// show tooltip on hover
$('.tooltip-trigger').hover(function() {
    var el = $(this);
    showTooltip(el);
}).mouseout(function() {
    hideTooltip();
});
// show tooltip on focus
$('.tooltip-trigger').focus(function() {
    var el = $(this);
    showTooltip(el);
}).blur(function() {
    hideTooltip();
});
// show tooltip on focus
$('.tooltip-trigger--click').click(function() {
    var el = $(this);
    showTooltip(el);
}).mouseout(function() {
    hideTooltip();
});



// POPOVERS ============================================================================================

function openPopover(el) {

    // close opened popovers
    closePopover($('.popover'));

    var $this = el;
    var $popover = $this.next('.popover');
    var position = $this.position();
    var width = $this.width();
    var height = $this.height();
    var margin_top = parseInt($this.css('marginTop'));
    var margin_left = parseInt($this.css('marginLeft'));
    var popover_height = $popover.height();
    var popover_width = $popover.width();

    $this.attr('aria-expanded', 'true'); // change aria for accessibility purposes

    $popover
        .addClass('active')
        .css({
            // Calculate position
            'top': position.top + margin_top - popover_height - 16, // position top + margin size - popover height - arrow
            'left': position.left + margin_left + (width / 2) - 22 // position left + margin size + half of the toggle width - arrow position
        })

    .find('.popover-arrow')
        .css({
            'left': - ((popover_width / 2) - 22)  // position arrow for xsmall media query
        });
}

function closePopover(el) {
    el.removeClass('active').prev('.popover-toggle').attr('aria-expanded', 'false');
}

// Toggle popovers
$('.popover-toggle').on('click', function () {
    var el = $(this);
    var $popover = el.next('.popover');

    if ($popover.hasClass('active')) {
        closePopover($popover);
    } else {
        openPopover(el);
    }
});

// Close popovers
$('.popover-close').on('click', function () {
    var el = $(this).closest('.popover');
    closePopover(el);

    // Focus on trigger
    el.prev('.popover-toggle').focus();
});






// NOTIFICATIONS =======================================================================================
$('.notification-close, .js-notification-close').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.notification').slideUp();
});



// SORTABLE ============================================================================================
$('.sort-item').on('click', function() {
    var $this = $(this);
    var $sort = $(this).attr("data-sort")

    if( $(this).hasClass("active") ){
        if( $(this).hasClass("asc") ){
            $(this).removeClass('asc').addClass('desc');
            $sort += "_desc";
        }
        else{
            $(this).removeClass('desc').addClass('asc');
            $sort += "_asc";
        }
    }
    else{
        $this.closest('.sort-list').find('.sort-item').removeClass('active asc desc');
        $this.addClass('active asc');
        $sort += "_asc";
    }

    if( $("#sort-selectbox").length ) {
        $('#sort-selectbox').val($sort);
    }
});




// Accessibility functions =============================================================================
$('[role="button"]').keydown(function(e) {
    var $this = $(this);
    // Check if ctrl key was pressed
    if (e.keyCode === 13) {
        $this.click();
    }
});


// Hide elements on blur ================================================================================
$(document).on('click', function() {
    hideTooltip();
    $('.dropdown').removeClass('active');
    $('.js-modify-search').removeClass('active');
});
$('.tooltip-toggle, .tooltip-hover, .tooltip, .dropdown, .js-modify-search').on('click', function(e) {
    e.stopPropagation();
});






// FORM SCRIPTS =========================================================================================

// Input only numbers
$(".js-only-numbers").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


// Focus input on click on input-prefix
$('.input-prefix').on('click', function() {
    $(this).siblings('.input').focus();
});

// Add spaces on every 3 characters
$('.js-add-input-spaces').on('input', function(){

    var start = this.selectionStart,
        end = this.selectionEnd,
        countSpaces = $(this).val().split(/ /g).length - 1;

    if ( $(this).val().length < 12 ) {

        $(this).val( $(this).val().replace(/\s/g, '').replace(/(.{3})/g,"$1 ") );

        var newCountSpaces = $(this).val().split(/ /g).length - 1;

        if(countSpaces != newCountSpaces){
            this.setSelectionRange(start, end);
        }
    }

});

// Slider component JQUery UI - It needs dynamic values.
var $usePointsValue = $(".use-points__value");
var $usePointsSlider = $(".use-points__slider");
var usePointsSliderMax = $usePointsSlider.data("max");

$usePointsValue.val($usePointsSlider.data("value"));
$usePointsSlider.slider({
    range: 'min',
    value: $usePointsSlider.data("value"),
    step: 1.00,
    min: 0.00,
    max: usePointsSliderMax, // This needs to be a dynamic value
    slide: function(event, ui) {

        if (ui.value > 0 && ui.value < 10) {
            $usePointsValue.val(10.00);
            $(".js-slider-popover-text").text("10.00");
        }
        else {
            $usePointsValue.val(ui.value.toFixed(2)).addClass("focus");
            $(".js-slider-popover-text").text(ui.value.toFixed(2));
        }
        removeErrorLabel($usePointsValue);
    },
    stop: function( event, ui ) {
        $usePointsValue.removeClass("focus");
        if (ui.value > 0 && ui.value < 10) {
            $usePointsSlider.slider("value", 10);
        }
    },
    create: function(event, ui) {
        $usePointsSlider.find('.ui-slider-handle').html("<div class='popover hidden-small popover-slider'><div class='popover-inner'><p><span class='js-slider-popover-text'>" + $usePointsSlider.data("value") + "</span> EUR</p></div><span class='popover-arrow'></span></div>");
    }
});

$usePointsValue.keyup(function() {

    var value = $(this).val();
    $usePointsSlider.slider("value", value);
    removeErrorLabel($usePointsValue);

    setTimeout(function() {

        //var value = $(this).val();
        removeErrorLabel($usePointsValue);

        if ( value <= usePointsSliderMax ) {
            $(".js-slider-popover-text").text(value);

            if ( value > 0 && value < 10 ) {
                createErrorLabel($usePointsValue, messageUsePointsLessThan10 +" EUR");
            }
        }
        else {
            $(".js-slider-popover-text").text(usePointsSliderMax);
            createErrorLabel($usePointsValue, messageUsePointsExceeded + usePointsSliderMax + " EUR");
        }
    }, 600)
});

// SCROLL TO FUNCTION =========================================================================================
jQuery.fn.scrollTo = function(elem, speed) {
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top - 40 + $(elem).offset().top
    }, speed == undefined ? 1000 : speed);
    return this;
};


$(".countries-list a.item[data-toggle]").on("click", function(e) {
  e.preventDefault();  // prevent navigating
  var selector = $(this).data("toggle");  // get corresponding element

  if(!$(selector).hasClass('active')) {
      $('.market-items-container.active').removeClass('active');
      $(selector).addClass('active');

  } else {
      $('.market-items-container.active').removeClass('active');
  }

});
