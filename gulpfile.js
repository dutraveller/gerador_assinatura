// var paths
var sass_path = 'src/sass/';



var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    //cmq         = require('gulp-combine-media-queries'),
    gcmq        = require('gulp-group-css-media-queries'),
    cssnano     = require('gulp-cssnano'),
    fileinclude = require('gulp-file-include'),
    rename      = require("gulp-rename"),
    uglify      = require("gulp-uglify"),
    flatten     = require("gulp-flatten"),
    notify      = require('gulp-notify'),
    plumber     = require('gulp-plumber'),
    watch       = require('gulp-watch'),
    svgo        = require('gulp-svgo');
    uncss       = require('gulp-uncss');    

    //inlinecss = require('gulp-inline-css');


// == Sass ==================================
gulp.task('styles', function() {

    gulp.src([sass_path +'main.scss', sass_path +'ie.scss', sass_path +'ie8.scss', sass_path +'print.scss'])
    //gulp.src([sass_path +'main.scss', sass_path +'ie.scss', sass_path +'ie8.scss', sass_path +'print.scss'])

        .pipe(plumber())

        .pipe(sass({
            style: 'expanded',
            onError: function(error) {
                notify({
                  title: "SASS ERROR",
                  message: "line " + error.line + " in " + error.file.replace(/^.*[\\\/]/, '') + "\n" + error.message
                }).write(error);
            }
        }))

        .pipe(gulp.dest('Content/Styles'))

        .pipe(gcmq())
        .pipe(gulp.dest('Content/Styles'))

        .pipe(cssnano())
        .pipe(gulp.dest('Content/Styles'))

        .pipe(uncss({
            html: ['src/assinatura.html'],
            ignore: [/active/, /inactive/, /is-active/, /button/, /expand-component/, /label-error/, /error/]
        }))
        .pipe(gulp.dest('Content/Styles'))

        .pipe(notify({
            title: "Gulp sass",
            message: "Great success! <%= file.relative %>"
        }))

});

// == Scripts ==================================
gulp.task('scripts', function() {
  return gulp.src('src/js/**/*.js')

    .pipe(uglify()) // minify code
    .pipe(flatten()) // move to a unique folder
    .pipe(gulp.dest('./Content/JS/vendor/'));
});



gulp.task('watch', function() {

    gulp.watch(sass_path + '**/*.scss', ['styles']);


    gulp.watch('src/js/vendor/**/*.js', ['scripts']);


});

gulp.task('default', ['watch'], function() {

});
